# Scrapy settings for PhiloWiki project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'PhiloWiki'

SPIDER_MODULES = ['PhiloWiki.spiders']
NEWSPIDER_MODULE = 'PhiloWiki.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'PhiloWiki (+http://www.yourdomain.com)'

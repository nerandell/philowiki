from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Request
from PhiloWiki.items import PhiloWikiItem

class PhiloWikiSpider(CrawlSpider):
    name = "mainSpider"
    allowed_domains = ["en.wikipedia.org"]
    start_urls = ["http://en.wikipedia.org/wiki/Fox_Entertainment_Group"]
    #rules = (Rule (SgmlLinkExtractor(allow=("/wiki/*", ),restrict_xpaths=("//div[@id='mw-content-text']/p[1]/a[1]",)), callback="parse_items", follow= True),)
    
    def parse(self, response):
        list = [];
        hxs = HtmlXPathSelector(response)
        title = hxs.select("//h1[@id='firstHeading']/span[1]/text()").extract()
        mainContent = hxs.select("//div[@id='mw-content-text']/p[1]/a[1]/@href").extract()
        list.append(mainContent[0])
        item = PhiloWikiItem()
        item['title'] = title[0]
        return Request("http://en.wikipedia.org" + mainContent[0],meta={'list':list,'item':item},callback=self.parse_items)

    def parse_items(self, response):
        list = response.request.meta['list']
        item = response.request.meta['item']
        hxs = HtmlXPathSelector(response)
        title = hxs.select("//h1[@id='firstHeading']/span[1]/text()").extract()
        mainContent = hxs.select("//div[@id='mw-content-text']/p[1]/a[1]/@href").extract()
        if(len(list)<=100 and mainContent[0]!='/wiki/Philosophy' and (mainContent[0] not in list)):
            list.append(mainContent[0])
            return Request("http://en.wikipedia.org" + mainContent[0],meta={'list':list, 'item':item},callback=self.parse_items)
        if(mainContent[0]=='/wiki/Philosophy'):
            list.append(mainContent[0])
            item['distance'] = len(list)
        else:
            item['distance'] = -1
        item['linklist'] = list
        return item
